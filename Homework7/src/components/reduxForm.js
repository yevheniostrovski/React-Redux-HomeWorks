import React from 'react';
import {reduxForm, Field, FormSection} from 'redux-form';
import {connect} from 'react-redux';

const UpperCaseNormalize = value => value && value.toUpperCase();

class ReduxFormComponent extends React.Component {

    render = () => {

        const {handleSubmit} = this.props;

        return (
            <div>
                <h1>HomeWork 7. Redux Form.</h1>
                <form onSubmit={handleSubmit}>
                    <div>
                        <label>First Name</label>
                        <Field name="firstname" component="input" normalize={UpperCaseNormalize} type="text"/>
                    </div>
                    <div>
                        <label>Second Name</label>
                        <Field name="secondname" component="input" type="text"/>
                    </div>
                    <FormSection name="education" className="flex-end">
                        <div>
                            <label>University</label>
                            <Field name="ed_university" component="input" type="text"/>
                        </div>
                        <div>
                            <label>City</label>
                            <Field name="ed_city" component="input" type="text"/>
                        </div>
                        <div>
                            <label>Country</label>
                            <Field name="ed_country" component="input" type="text"/>
                        </div>
                    </FormSection>
                    <FormSection name="address" className="flex-end">
                        <div>
                            <label>Country</label>
                            <Field name="ad_country" component="input" type="text"/>
                        </div>
                        <div>
                            <label>City</label>
                            <Field name="ad_city" component="input" type="text"/>
                        </div>
                        <div>
                            <label>Street</label>
                            <Field name="ad_street" component="input" type="text"/>
                        </div>
                    </FormSection>
                    {/*<Field name="step" component={CustomInput} type="text"/>*/}
                    <button>Submit</button>
                </form>
            </div>
        )
    }
}

// -- Redux --
const mapStateToProps = (state, ownProps) => {
    return {
        users: state.users,
        initialValues: {
            firstname: 'Yevheni',
            secondname: 'Ostrovskyi'
        }
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch
    };
};

const ConnectedForm = connect(
    mapStateToProps,
    mapDispatchToProps
)(
    reduxForm({
        form: 'simpleForm',
        onSubmit: values => console.log("values", values),
    })(ReduxFormComponent)
);


export default ConnectedForm;
