import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Helmet from 'react-helmet';
import { Switch, Route, Link, NavLink } from 'react-router-dom';
import Spinner from './Spinner.svg';
import ReduxForm from './reduxForm';
import FormData from './formData';

class App extends Component {
  componentDidMount(){
    let { getUsers } = this.props;
    getUsers();
  }
  render() {
    let { loaded, data } = this.props.users;

    if( loaded === true ){
      return (
        <div className="App">
         <Helmet>
            <html lang="en" />
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <title>Main Component</title>
          </Helmet>

          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React</h1>
          </header>
          <div className="AppNavigation">
            <NavLink activeClassName="active" className="AppNavigation__links" to="/formdata">FormData</NavLink>
            <NavLink activeClassName="active" className="AppNavigation__links" to="/redux">Redux</NavLink>
          </div>
          <ul>
          </ul>
          <Switch>
            <Route path="/formdata" component={FormData}/>
            <Route path="/redux" component={ReduxForm}/>
          </Switch>

        </div>
      );
    } else {
      return(<div className="wv"><img src={Spinner}/></div>);
    }
  }
}

export default App;
