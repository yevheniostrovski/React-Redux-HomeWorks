import { combineReducers } from 'redux';


const People_InitialState = {
    data: [],
    loaded: false,
    loading: false,
    error: null
};

function people(state = People_InitialState, action){
    switch (action.type) {
        case 'GET_DATA_SUCCESS':
            return {
                ...state,
                data: action.data
            }

        case 'GET_DATA_START':
            return {
                ...state,
                loading: true
            }

        case 'RESPONSE':
            return {
                ...state,
                data: action.data,
                loading: false,
                loaded: true
            };

        case 'GET_DATA_ERROR':
            return {
                ...state,
                error: action.error
            };
        default:
            return state;
    }
};


const reducer = combineReducers({
    people
});

export default reducer;
