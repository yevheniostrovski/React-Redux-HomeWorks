const demoActionCreator = {
    doSomsng: () => {
        return function (dispatch, getState) {
            dispatch({
                type: 'ACTION_TYPE',
                payload: ['some', 'data']
            });
        };
    },
    getData: () => {
        return function (dispatch, getState) {
            const url = "http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2";

            dispatch({
                type: 'GET_DATA_START',
                data: fetch(url)
                    .then(res => res.json())
                    .then(response => {
                        dispatch({
                            type: 'GET_DATA_SUCCESS',
                            data: response
                        });
                    }).catch( err => {
                        dispatch({
                            type: 'GET_DATA_ERROR_',
                            error: err
                        })
                    })
            })
        }
    },
    fetchPromise: () => {
        return function (dispatch, getState) {
            const url = "http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2";
            dispatch({
                type: 'PROMISE',
                actions: ['REQUEST','RESPONSE','ERROR'],
                promise: fetch(url).then( res => res.json() )
            });
        };
    }
};

export default demoActionCreator;
