import React, {Component} from 'react';
import People from './People.js'
import logo from './logo.svg';
import './App.css';
import {Switch, Route, Link, NavLink} from 'react-router-dom';

class App extends Component {
    render() {
        return (
            <div>
                <header className="appHeader">
                    <Link to="/">Main</Link>
                    <Link to="/people">People</Link>
                </header>
                <Switch>
                    <Route path="/" exact
                        render = { () =>
                        (
                            <div className="start">
                                <img className="App-logo" width="100" src={logo}/>
                                <h2>It's a Main Page</h2>
                            </div>
                        )}
                    />
                    <Route path="/people" component={People}/>
                </Switch>
            </div>
        )
    }
}

export default App;
