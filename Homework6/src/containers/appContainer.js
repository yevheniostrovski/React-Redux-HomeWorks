import { connect } from 'react-redux';

// IMPORT YOUR REACT COMPONENT
import App from '../components/App';
// import People from '../components/People';
// IMPORT YOUR ACTIONS
import demoActionCreator from '../actions/demoActionCreator';

const mapStateToProps = (store, ownProps) => {
    return {
        ourData: store.people.data
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch,
        // getData: () => {
        //     dispatch( demoActionCreator.getData() );
        // },
        addData: ( array ) => {
            dispatch( demoActionCreator.doSomsng(array) );
        },
        getData: () => {
            dispatch( demoActionCreator.getData() );
        }
    };
};

const MyApp = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

export default MyApp;
