import React, { Component } from 'react';
import Guests from './guests.json';
import logo from './logo.svg';
import './App.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';

class ListItem extends Component {
    state = {
        arrived: true
    }
    arrived = (event) => {
        console.log( event, this.state );
        let status = this.state.arrived === true ? false : true;
        this.setState({ arrived: status });
    }
    render(){
        console.log("List-Item rendered");
        return(
            <div className={this.state.arrived === true ? "myList_Item" : "myList_Item arrived" }>
                <div className="infoWrap">
                    <p>Гость <b>{this.props.item.name}</b> работает в компании <b>"{this.props.item.company}"</b>.</p>
                    <p>Его контакты:</p>
                    <p><b>{this.props.item.phone}</b></p>
                    <p><b>{this.props.item.address}</b></p>
                </div>
                <div className="arrivedBtn" onClick={this.arrived}>Прибыл</div>
            </div>
        );
    }
}

class App extends Component {
    state = {
        contactsInfo: Guests
    };
    constructor(props){
        super(props);
        this.state = {
            contactsInfo: Guests
        };
    }
    searchGuestsByClick() {
        console.log("click test");
    }
    filterGuests = (event) => {
        let searchQuery = event.target.value.toLowerCase();
        let renderContacts = Guests.filter( (item) => {
            let itemName = item.name.toLowerCase();
            return itemName.indexOf(searchQuery) !== -1;
        });
        this.setState({ contactsInfo: renderContacts});
    }
    render(){
        let { contactsInfo } = this.state;
        return <div className="App">
                  <div className="App-header">
                      <div className="header-inner-wrap">
                          <h1 className="App-title">Список гостей</h1>
                          <span className="searchBtn" onClick={this.searchGuestsByClick}><i className="fa fa-search"></i></span>
                      </div>
                      <input className="searchInput" onChange={this.filterGuests} type="text" placeholder="Введите имя гостя для поиска"/>
                  </div>
                  <div className="myList"> {
                      contactsInfo.length !== 0 ?
                          contactsInfo.map( (item, key) => {
                              return(
                                  <ListItem key={key} item={item} />
                              );
                          })
                          :
                          <div className="notFound">Упс! По вашем запросу ничего не найдено.</div>
                  }
                  </div>
                </div>;
    }
}

export default App;
