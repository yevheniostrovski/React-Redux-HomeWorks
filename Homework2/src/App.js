import React, { Component } from 'react';
import Tabs from "./components/Tabs";
import Tab from "./components/Tab"
import './App.css';

class App extends Component {
  render() {
    return(
        <Tabs>
            <Tab linkText={'One'}
                 linkClassName={'first-link'}>
                <p>First content</p>
            </Tab>
            <Tab linkText={'Two'}
                 linkClassName={'second-link'}>
                <p>Second content</p>
            </Tab>
            <Tab linkText={'Three'}
                 linkClassName={'third-link'}>
                <p>Third content</p>
            </Tab>
            <Tab linkText={'Four'}
                 linkClassName={'fourth-link'}>
                <p>Fourth content</p>
            </Tab>
        </Tabs>
    );
  }
}

export default App;
