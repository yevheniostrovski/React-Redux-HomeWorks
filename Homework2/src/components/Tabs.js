import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Tabs extends Component {
    constructor(props, context){
        super(props, context);
        this.state = {
            activeTabIndex: 0
        };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(tabIndex) {
        this.setState({
            activeTabIndex: tabIndex === this.state.activeTabIndex ? null : tabIndex
        })
    };

    printChildrenItems() {
        return React.Children.map(this.props.children, (childrenItem, index) => {
            return React.cloneElement(childrenItem, {
                tabIndex: index,
                isActive: index === this.state.activeTabIndex,
                onClick: this.handleClick
            });
        });
    };

    printContent() {
        const {children} = this.props;
        const {activeTabIndex} = this.state;

        if(children[activeTabIndex]){
            return children[activeTabIndex].props.children;
        }
    };

    render() {
        return (
            <header className="tabs">
                <ul className="tabs-nav">
                    {this.printChildrenItems()}
                </ul>
                <div className="tabs-content">
                    {this.printContent()}
                </div>
            </header>
        )
    };
}

export default Tabs;