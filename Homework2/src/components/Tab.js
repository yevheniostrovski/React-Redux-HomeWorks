import React, { Component } from 'react';

class Tab extends Component {
    constructor(props, context){
        super(props, context);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) {
        event.preventDefault();
        this.props.onClick(this.props.tabIndex);
    }

    render() {
        return (
            <li className={`tab ${this.props.linkClassName} ${this.props.isActive ? 'active' : ''}`}
                onClick={this.handleClick}>
                {`${this.props.linkText}`}
            </li>
        );
    }
}

export default Tab;