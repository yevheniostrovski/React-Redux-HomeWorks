import React, { Component } from 'react';

class DemoRef extends Component{

  inputRef = React.createRef();

  handler = (e) => {
    console.log(e.target);
  }

  componentDidMount = () => {
    console.log( this.inputRef.current );

  }

  render = () => {
    let { handler } = this;
    return(
      <div>
        {
          <input
          ref={this.inputRef}
          onChange={handler}
          placeholder="Text"
        />
        }
      </div>
    );
  }
}

export default DemoRef;
