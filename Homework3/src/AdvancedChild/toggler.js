import React, {Component} from 'react';


export class Toggler extends Component {
    render() {
        let {name, children, activeToggler, changeStatus, changeStatusGender} = this.props;

        return (
            <div>
                {this.props.name}
                <div className="togglerContainer">
                    {
                        React.Children.map(
                            children,
                            (ChildrenItem) => {
                                if (ChildrenItem.props.name === activeToggler) {
                                    return React.cloneElement(ChildrenItem, {
                                        name: ChildrenItem.props.name,
                                        active: true,
                                        changeStatus: changeStatus,
                                        changeStatusGender: changeStatus
                                    })
                                } else {
                                    return React.cloneElement(ChildrenItem, {
                                        name: ChildrenItem.props.name,
                                        changeStatus: changeStatus,
                                        changeStatusGender: changeStatusGender
                                    })
                                }
                            }
                        )
                    }
                </div>
            </div>
        );
    }
}

export const TogglerLayout = ({name, active, changeStatus}) => {
    return (
        <div className={
            active === true ?
                "togglerItem active" :
                "togglerItem"
        }
             data-value={name}
             onClick={
                 changeStatus !== undefined ?
                     changeStatus :
                     null
             }
        >
            {name}
        </div>
    );
};

export const TogglerGender = ({name, active, changeStatus}) => {
    return (
        <div className={
            active === true ?
                "togglerItem active" :
                "togglerItem"
        }
             data-value={name}
             onClick={
                 changeStatus !== undefined ?
                     changeStatus :
                     null
             }
        >
            {name}
        </div>
    );
};
