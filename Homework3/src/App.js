import React, { Component } from 'react';
import './style/App.css';



// import CompWithPropTypes from './PropTypes/';
import ControlledForm from './ControlledForm';

// import AdvancedChild from './AdvancedChild';
// import CompWithPropTypes from "./PropTypes";

const ComponentToSend = () => (<div>Null</div>);


class App extends Component {
  state = {
    textDemo: 'Homework3'
  }
  changeHandler = (e) => {
     let value = e.target.value;
     this.setState({
       textDemo: value
     })
   }
  render = () => {
    return(
      <div>
        <h1> Lesson 3. Controlled Form</h1>

          <ControlledForm />

      </div>
    );
  }
}
/*

*/
export default App;
