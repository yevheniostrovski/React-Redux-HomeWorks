import React, {Component} from 'react';
import {Toggler, TogglerLayout, TogglerGender} from '../AdvancedChild/toggler';

class ControlledForm extends Component {

    state = {
        data: {
            user: "",
            password: "",
            lang: "",
            age: "",
            activeToggler: "left",
            activeTogglerGender: "male"
        }
    };

    changeStatus = (event) => {
        let TogglerValue = event.target.dataset.value;
        this.setState({
            data: {
                ...this.state.data,
                activeToggler: TogglerValue,
            }
        });
    }

    changeStatusGender = (event) => {
        let TogglerValue = event.target.dataset.value;
        this.setState({
            data: {
                ...this.state.data,
                activeTogglerGender: TogglerValue
            }
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log('submitted data', this.state.data);
    }

    handleFormChange = event => {
        let value = event.target.value;
        let name = event.target.name;

        this.setState({
            data: {
                ...this.state.data,
                [name]: value
            }
        });
    }
    render = () => {
        let {activeToggler, activeTogglerGender} = this.state.data;
        return (
            <div className="App">

                <form onSubmit={this.handleSubmit} autoComplete="off">
                    <label>
                        <div>User name</div>
                        <input
                            type="text"
                            name="user"
                            onChange={this.handleFormChange}
                            autocomplite="false"
                        />
                    </label>
                    <label>
                        <div>Password</div>
                        <input
                            type="password"
                            name="password"
                            onChange={this.handleFormChange}
                        />
                    </label>
                    <label>
                        <div>Age</div>
                        <input
                            type="age"
                            name="age"
                            onChange={this.handleFormChange}
                        />
                    </label>

                    <Toggler
                        name="Choose layout"
                        activeToggler={activeToggler}
                        changeStatus={this.changeStatus}
                    >
                        <TogglerLayout name="left"/>
                        <TogglerLayout name="center"/>
                        <TogglerLayout name="right"/>
                    </Toggler>

                    <label>
                        <div>Favorite programming language</div>
                        <input
                            type="text"
                            name="lang"
                            onChange={this.handleFormChange}
                        />
                    </label>

                    <Toggler
                        name="Gender"
                        activeToggler={activeTogglerGender}
                        changeStatus={this.changeStatusGender}
                    >
                        <TogglerGender name="male"/>
                        <TogglerGender name="female"/>
                    </Toggler>


                    <button type="submit">Send</button>
                </form>
            </div>
        )
    }

}


export default ControlledForm;
